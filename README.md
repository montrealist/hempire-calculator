# Hempire Calculator

Simple WordPress plugin. Creates a shortcode that outputs a insulation work estimate calculator into post content.

http://hempire.com.ua/

## Notes

Notion: https://www.notion.so/montrealist/hempire-house-image-in-sketch-a6058b60317a4d799d93094d876f8f66

Codepen: https://codepen.io/montrealist/full/BapZWby

## Usage

Simply paste this shortcode into any WordPress page or post content: `[hempire_calculator]`