/**
 * Displays an alert box with our first translated message when called.
 */
function pdev_show_alert_box_1() {
    alert( hempire_calculator_L10n.pdev_box_1 );
}
             
/**
 * Displays an alert box with our second translated message when called.
 */
function pdev_show_alert_box_2() {
    alert( hempire_calculator_L10n.pdev_box_2 );
}

// jQuery noConflict wrapper
(function($) {
    const sections = $("svg.house .section");
    const buttons = $("#buttons-sections li a");
    // console.log('sections', sections);
    
    const coeff_floors = 1.3;
      $(this).toggleClass("on");
      const button = buttons.filter('[data-section="' + $(this).attr('id') + '"]');
      button.toggleClass('active');
    });
    
    
    buttons.on("click", function() {
      $(this).toggleClass("active");
      const sec = $(this).data( "section" );
      const res = sections.filter("#" + sec);
      if(res.length) {
        res.toggleClass('on');
      }
      return false;
    });
    
    $('#button-calculate').on("click", function() {
      const secs = sections.filter(".on");
      const area = $('#input-area').val();
      if(secs.length && area) {
        const floors = $('#input-floors option:selected').val();
        // console.log('floors: ', floors, ' area: ', area);
        let res = 0;
        res += floors * coeff_floors * area;
        $.each( secs, function( key, elem ) {
          const coefficient = $(elem).data('coeff');
          res = res * coefficient;
        });
        alert('Cost is ₴ ' + Math.round(res) * 50 + '.');
      } else {
        alert('Select at least one section and provide a number for area!');
      }
      return false;
    });   
})(jQuery);