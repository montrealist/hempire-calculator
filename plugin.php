<?php

/**
 * Plugin Name: Hempire Calculator
 * Plugin URI: http://hempire.com.ua/en/
 * Description: Creates a shortcode that outputs a insulation work estimate calculator into post content.
 * Version: 0.1
 * Author: Max Kay (max@liro.ca)
 * Author URI: http://liro.ca/
 * Text Domain: hempire-calculator
 * Domain Path: /languages
 */

add_action('init', 'hempire_register_calculator_shortcode');

// Register shortcodes.
function hempire_register_calculator_shortcode()
{
    add_shortcode('hempire_calculator', 'hempire_calculator_shortcode');
}

// Shortcode callback function.
function hempire_calculator_shortcode()
{

    $html = '';

    ob_start();
    include(plugin_dir_path(__FILE__) . '/hempire-calculator-html.php');
    $html .= ob_get_contents();
    ob_end_clean();

    return $html;
}

/* Add the translation function after the plugins loaded hook. */
// add_action( 'plugins_loaded', 'hempire_calculator_load_translation' );

/**
 * Loads a translation file if the paged being viewed isn't in the admin.
 *
 * @since 0.1
 */
function hempire_calculator_load_translation()
{

    /* If we're not in the admin, load any translation of our plugin. */
    if (!is_admin())
        load_plugin_textdomain('hempire-calculator', false, basename(dirname(__FILE__)) . '/languages/');
}

function hempire_calculator_enqueue_scripts()
{
    wp_register_style( 'hempire_calculator_stylesheet', plugins_url( 'hempire-calculator/style.css' ), null, '0.0.1', 'all' );
    wp_enqueue_style( 'hempire_calculator_stylesheet' );

    $script = trailingslashit(plugins_url('hempire-calculator')) . 'hempire-calculator-script.js';
    wp_register_script(
        'hempire-calculator',
        $script,
        array('jquery'),
        '1.0.0',
        true
    );

    wp_enqueue_script('hempire-calculator');

    /* Localize text strings used in the JavaScript file. */
    wp_localize_script('hempire-calculator', 'hempire_calculator_L10n', array(
        'pdev_box_1' => __('Alert boxes are annoying!', 'hempire-calculator'),
        'pdev_box_2' => __('They are really annoying!', 'hempire-calculator'),
    ));
}
add_action('wp_enqueue_scripts', 'hempire_calculator_enqueue_scripts');

/* Add our script function to the print scripts action. */
// add_action( 'wp_print_scripts', 'hempire_calculator_load_script' );

/**
 * Loads the alert box script and localizes text strings that need translation.
 *
 * @since 0.1
 */
function hempire_calculator_load_script()
{

    /* If we're in the WordPress admin, don't go any farther. */
    if (is_admin())
        return;

    /* Get script path and file name. */
    $script = trailingslashit(plugins_url('hempire-calculator')) . 'hempire-calculator-script.js';

    /* Enqueue our script for use. */
    wp_enqueue_script('hempire-calculator', $script, false, 0.1);

    /* Localize text strings used in the JavaScript file. */
    wp_localize_script('hempire-calculator', 'hempire_calculator_L10n', array(
        'pdev_box_1' => __('Alert boxes are annoying!', 'hempire-calculator'),
        'pdev_box_2' => __('They are really annoying!', 'hempire-calculator'),
    ));
}

/* Add our alert box buttons to the site footer. */
// add_action('wp_footer', 'hempire_calculator_display_buttons');

/**
 * Displays two input buttons with a paragraph.  Each button has an onClick()
 * event that loads a JavaScript alert box.
 *
 * @since 0.1
 */
function hempire_calculator_display_buttons()
{

    /* Get the HTML for the first input button. */
    $hempire_calculator_buttons = '<input type="button" onclick="pdev_show_alert_box_1()"
    value="' . esc_attr__('Press me!', 'hempire-calculator') . '" />';

    /* Get the HTML for the second input button. */
    $hempire_calculator_buttons .= '<input type="button" onclick="pdev_show_alert_box_2()"
    value="' . esc_attr__('Now press me!', 'hempire-calculator') . '" />';

    /* Wrap the buttons in a paragraph tag. */
    echo '<p>' . $hempire_calculator_buttons . '</p>';
}
